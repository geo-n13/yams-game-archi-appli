# Yam Master Project

## Overview

Yam Master is a two-player dice game where each player takes turns rolling five dice, aiming to achieve specific combinations to score points. This project consists of three main parts: the WebSocket server, the authentication server, and the client application.

### Game Rules

Each turn, a player can roll the dice up to three times to achieve a desired combination. After each roll, the player can set aside any number of dice and re-roll the others. Dice set aside can be re-rolled in subsequent rolls.

#### Combinations

- **Three of a Kind**: Three dice showing the same number (e.g., three "2s").
- **Full House**: A combination of three of a kind and a pair.
- **Four of a Kind**: Four dice showing the same number.
- **Yam**: All five dice showing the same number.
- **Straight**: A sequence of five dice (1-2-3-4-5 or 2-3-4-5-6).
- **≤8**: The sum of the dice must be 8 or less.
- **Instant Win**: Achieve any combination (except three of a kind) on the first roll.
- **Challenge**: Declare a challenge before the second roll, and achieve any combination (except three of a kind) in the next two rolls.

Dice can form multiple combinations simultaneously (e.g., a Yam is also a three of a kind, four of a kind, and a full house). The player chooses which combination to use.

When a player completes a combination, they can place a token on an available corresponding space on the board. Players can use the Yam Predator to remove an opponent's token by rolling a Yam.

## Tech Stack

- **Server:** Socket.io, Express
- **Database:** MongoDB
- **Client:** Expo

## Key Features

- Real-time multiplayer functionality via WebSocket server.
- Authentication server with JWT tokens.
- Client application built with Expo.

## Getting Started

### Prerequisites

Ensure you have the following installed on your machine:
- Node.js v20.11.1
- MongoDB v7.2

### Installation

#### 1. Clone the Repository

First, clone the project repository:

```bash
git clone https://gitlab.com/geo-n13/yams-game-archi-appli.git
```

#### 2. Navigate to the Project Directories

Move into each directory to set up the WebSocket server, authentication server, and client:

```bash
cd yams-game-archi-appli/websocket-server
```

### Part 1: WebSocket Server

#### Install Dependencies

Install the project dependencies:

```bash
npm ci
```

#### Start the WebSocket Server

Open a terminal and run the following command:

```bash
npm run start
```

The WebSocket server will start on port 3001.

### Part 2: Authentication Server

Navigate to the authentication server directory:

```bash
cd ../auth-server
```

#### Install Dependencies

Install the project dependencies:

```bash
npm ci
```

#### Configuration

Create a `.env` file at the root of the auth-server directory and set the following environment variables:

```dotenv
JWTKEY=YourSecretKey
MONGODB_URI=YourMongoDBUri
MONGODB_DATABASE=yams_master
FRONTEND_URL=http://localhost:8081
NODE_ENV=development
API_HOSTNAME=localhost
PORT=3002
```

#### Start the Authentication Server

Open a terminal and run the following command:

```bash
npm run start
```

The authentication server will start on port 3002. You can access it at http://localhost:3002/api/auth/login.

### Part 3: Client

Navigate to the client directory:

```bash
cd ../client
```

#### Install Dependencies

Install the project dependencies:

```bash
npm install
```

#### Configuration

Create a `.env` file at the root of the client directory and set the following environment variable:

```dotenv
EXPO_PUBLIC_AUTH_API_URL=http://localhost:3002
```

#### Start the Client

Open a terminal and run the following command:

```bash
npx expo start
```

The client application will run on port 8081.

## Authors

Developed by:
- [@geo-n13](https://gitlab.com/geo-n13)